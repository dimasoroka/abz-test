import React from 'react';
import NavBar from '../NavBar/NavBar';
import Button from '../Button/Button';
import headerBg from '../../assets/img/header-img.jpg';
import './styles.scss';

const Header = () => (
  <header className="header">
    <NavBar />
    <div
      className="header__wrapper"
      style={{
        background: `linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${headerBg}) no-repeat center center / cover`,
      }}
    >
      <div className="header__wrap">
        <h1 className="header__title">
          Test assignment for front-end developer
        </h1>
        <p className="header__text">
          What defines a good front-end developer is one that has skilled
          knowledge of HTML, CSS, JS with a vast understanding of User design
          thinking as they'll be building web interfaces with accessibility in
          mind. They should also be excited to learn, as the world of Front-End
          Development keeps evolving.
        </p>
        <Button text="Sign up" />
      </div>
    </div>
  </header>
);

export default Header;
