import React, { useState, useEffect } from 'react';
import getPositions from '../../services/getPositions';
import RadioItem from '../RadioItem/RadioItem';
import Loader from '../Loader/Loader';
import './styles.scss';

const RadioList = () => {
  const [positions, setPositions] = useState([]);

  useEffect(() => {
    getPositions().then((response) => {
      setPositions(response);
    });
  }, []);

  return (
    <div className="radio-list">
      <p className="radio-list__title">Select your position</p>
      {positions.length > 1 ? (
        positions.map((position) => {
          return <RadioItem item={position} key={position.id} />;
        })
      ) : (
        <Loader />
      )}
    </div>
  );
};

export default RadioList;
