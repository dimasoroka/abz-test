import React from 'react';
import Button from '../Button/Button';
import './styles.scss';

const Menu = () => (
  <div className="menu">
    <Button text="Users" />
    <Button text="Sign up" />
  </div>
);

export default Menu;
