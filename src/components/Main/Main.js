import React, { useState } from 'react';
import UserContext from '../../Context/UserContext';
import UsersList from '../UsersList/UsersList';
import Form from '../Form/Form';
import './styles.scss';

const Main = () => {
  const [usersCount, setUsersCount] = useState(6);

  return (
    <UserContext.Provider value={{ usersCount, setUsersCount }}>
      <main className="main">
        <section className="get">
          <UsersList />
        </section>
        <section className="post">
          <Form />
        </section>
      </main>
    </UserContext.Provider>
  );
};

export default Main;
