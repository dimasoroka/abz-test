import React, { useContext } from 'react';
import RadioContext from '../../Context/RadioContext';
import './styles.scss';

const RadioItem = ({ item }) => {
  const { valueInpPosition, setValueInpPosition } = useContext(RadioContext);

  return (
    <label className="radio__item">
      <input
        name="position"
        type="radio"
        onChange={() => setValueInpPosition(item.id)}
        className="radio__inp"
        checked={item.id === valueInpPosition && true}
      />
      <span className="radio__name">{item.name}</span>
    </label>
  );
};

export default RadioItem;
