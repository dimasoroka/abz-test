import React, { useEffect, useState, useContext } from 'react';
import UserContext from '../../Context/UserContext';
import RadioContext from '../../Context/RadioContext';
import getToken from '../../services/getToken';
import postUsers from '../../services/postUsers';
import Heading from '../Heading/Heading';
import FileInput from '../FileInput/FileInput';
import RadioList from '../RadioList/RadioList';
import Button from '../Button/Button';
import Loader from '../Loader/Loader';
import SuccessForm from '../SuccessForm/SuccessForm';
import './styles.scss';

const Form = () => {
  const { setUsersCount } = useContext(UserContext);

  const [token, setToken] = useState('');
  const [btnDisabled, setBtnDisabled] = useState(true);
  const [sendingUser, setSendingUser] = useState(false);
  const [successForm, setSuccessForm] = useState(false);

  const [valueInpName, setValueInpName] = useState(null);
  const [valuInpEmail, setValuInpEmail] = useState(null);
  const [valueInpPhone, setValueInpPhone] = useState(null);
  const [valueInpPosition, setValueInpPosition] = useState(1);
  const [valueInpImg, setValueInpImg] = useState(null);

  const [nameInpError, setNameInpError] = useState(false);
  const [emailInpError, setEmailInpError] = useState(false);
  const [phoneInpError, setPhoneInpError] = useState(false);
  const [imgInpError, setImgInpError] = useState(false);

  const rgxEmail =
    /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
  const rgxPhone = /^[\+]{0,1}380([0-9]{9})$/;

  useEffect(() => {
    if (valueInpName && valuInpEmail && valueInpPhone && valueInpImg) {
      setBtnDisabled(false);
    } else {
      setBtnDisabled(true);
    }
  });

  useEffect(() => {
    getToken().then((response) => {
      setToken(response);
    });
  }, []);

  const checkName = (value) => {
    if (value.length >= 2 && value.length <= 60) {
      setValueInpName(value);
      setNameInpError(false);
    } else {
      setValueInpName('');
      setNameInpError(true);
    }
  };

  const checkEmail = (value) => {
    if (value.match(rgxEmail)) {
      setValuInpEmail(value);
      setEmailInpError(false);
    } else {
      setValuInpEmail('');
      setEmailInpError(true);
    }
  };

  const checkPhone = (value) => {
    if (value.match(rgxPhone)) {
      setValueInpPhone(value);
      setPhoneInpError(false);
    } else {
      setValueInpPhone('');
      setPhoneInpError(true);
    }
  };

  const checkImg = (value) => {
    setValueInpImg(value.files[0]);
    if (value.files[0]) {
      setImgInpError(false);
    } else {
      setImgInpError(true);
    }
  };

  const handleSubmitForm = (form) => {
    form.preventDefault();

    const formData = new FormData();
    formData.append('position_id', valueInpPosition);
    formData.append('name', valueInpName);
    formData.append('email', valuInpEmail);
    formData.append('phone', valueInpPhone);
    formData.append('photo', valueInpImg);

    if (valueInpName && valuInpEmail && valueInpPhone && valueInpImg) {
      setSendingUser(true);
      postUsers(formData, token).then((response) => {
        if (response.data.success === true) {
          setSendingUser(false);
          setUsersCount(6);
          setSuccessForm(true);
        }
      });
    }
  };

  return (
    <RadioContext.Provider value={{ valueInpPosition, setValueInpPosition }}>
      {!successForm ? (
        !sendingUser ? (
          <div className="form-wrapper">
            <Heading title="Working with POST request" />
            <form
              onSubmit={(e) => {
                handleSubmitForm(e);
              }}
              className="form"
            >
              <div className={`form__input-wrap ${nameInpError && 'error'}`}>
                <input
                  type="text"
                  placeholder="Your name"
                  onChange={(e) => checkName(e.target.value)}
                  className={'form__input'}
                />
                <p className="form__error">Write name</p>
              </div>
              <div className={`form__input-wrap ${emailInpError && 'error'}`}>
                <input
                  type="text"
                  placeholder="Email"
                  onChange={(e) => checkEmail(e.target.value)}
                  className="form__input"
                />
                <p className="form__error">Write email</p>
              </div>
              <div className={`form__input-wrap ${phoneInpError && 'error'}`}>
                <input
                  type="text"
                  placeholder="Phone"
                  onChange={(e) => checkPhone(e.target.value)}
                  className="form__input"
                />
                <p className="form__error">Write phone</p>
              </div>
              <div className="form__input-wrap">
                <RadioList />
                <div className="form__error">Select specialization</div>
              </div>
              <div className={`form__input-wrap ${imgInpError && 'error'}`}>
                <FileInput change={(file) => checkImg(file)} />
                <p className="form__error">Add photo</p>
              </div>
              <Button text="Sign up" disabled={btnDisabled} />
            </form>
          </div>
        ) : (
          <Loader />
        )
      ) : (
        <SuccessForm />
      )}
    </RadioContext.Provider>
  );
};

export default Form;
