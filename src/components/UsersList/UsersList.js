import React, { useEffect, useState, useContext } from 'react';
import UserContext from '../../Context/UserContext';
import getNews from '../../services/getUsers';
import Button from '../Button/Button';
import Loader from '../Loader/Loader';
import UsersItem from '../UsersItem/UsersItem';
import Heading from '../Heading/Heading';
import './styles.scss';

const UsersList = () => {
  const { usersCount, setUsersCount } = useContext(UserContext);

  const [users, setUsers] = useState([]);
  const [totalUsersCount, setTotalUsersCount] = useState('');

  useEffect(() => {
    getNews(usersCount).then((response) => {
      setTotalUsersCount(response.total_users);
      setUsers(response.users);
    });
  }, [usersCount]);

  const showMoreUsers = () => {
    setUsersCount(usersCount + 6);
  };

  return (
    <div className="users">
      <Heading title="Working with GET request" />
      <ul className="users__list">
        {users.length > 1 ? (
          users.map((user) => <UsersItem user={user} key={user.id} />)
        ) : (
          <Loader />
        )}
      </ul>
      {usersCount < totalUsersCount && (
        <Button
          text="Show more"
          onClick={() => {
            showMoreUsers();
          }}
        />
      )}
    </div>
  );
};

export default UsersList;
