import React, { useState } from 'react';
import './styles.scss';

const FileInput = ({ change }) => {
  const [fileName, setFileName] = useState('No file chosen');

  return (
    <label className="file">
      <input
        type="file"
        accept=".jpg, .jpeg, .png"
        className="file__inp"
        onChange={(inp) => {
          change(inp.target);
          inp.target.files[0]
            ? setFileName(inp.target.files[0].name)
            : setFileName('No file chosen');
        }}
      />
      <span className="file__btn">Upload</span>
      <span className="file__text">{fileName}</span>
    </label>
  );
};

export default FileInput;
