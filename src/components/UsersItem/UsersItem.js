import React from 'react';
import './styles.scss';

const UsersItem = ({ user }) => (
  <li className="users__item">
    <img className="users__img" src={user.photo} alt={user.name} />
    <p className="users__name" title={user.name}>
      {user.name}
    </p>
    <p className="users__position" title={user.position}>
      {user.position}
    </p>
    <p className="users__email" title={user.email}>
      {user.email}
    </p>
    <p className="users__phone" title={user.phone}>
      {user.phone}
    </p>
  </li>
);

export default UsersItem;
