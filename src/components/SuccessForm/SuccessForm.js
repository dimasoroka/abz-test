import React from 'react';
import SuccessImg from '../../assets/img/success-image.svg';
import Heading from '../Heading/Heading';
import './styles.scss';

const SuccessForm = () => (
  <>
    <Heading title="User successfully registered" />
    <img src={SuccessImg} alt="success form" className="success-img" />
  </>
);

export default SuccessForm;
