import React from 'react';
import './styles.scss';

const Heading = ({ title }) => <h1 className="title">{title}</h1>;

export default Heading;
