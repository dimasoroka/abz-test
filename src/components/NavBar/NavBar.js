import React from 'react';
import Logo from '../Logo/Logo';
import Menu from '../Menu/Menu';
import './styles.scss';

const NavBar = () => (
  <nav className="nav">
    <div className="nav__wrapper">
      <Logo />
      <Menu />
    </div>
  </nav>
);

export default NavBar;
