import React from 'react';
import logoImg from '../../assets/img/logo.svg';

const Logo = () => <img src={logoImg} alt="logotype" />;

export default Logo;
