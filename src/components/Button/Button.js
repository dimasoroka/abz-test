import React from 'react';
import './styles.scss';

const Button = ({ text, onClick, disabled = false }) => (
  <button onClick={onClick} className="btn" disabled={disabled}>
    {text}
  </button>
);

export default Button;
