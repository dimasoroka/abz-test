import axios from 'axios';

const getPositions = async () => {
  return await axios
    .get('https://frontend-test-assignment-api.abz.agency/api/v1/positions')
    .then((response) => response.data.positions);
};

export default getPositions;
