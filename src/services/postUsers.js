import axios from 'axios';

const postUsers = (formData, token) => {
  return axios.post(
    'https://frontend-test-assignment-api.abz.agency/api/v1/users',
    formData,
    {
      headers: {
        Token: token,
      },
    }
  );
};

export default postUsers;
