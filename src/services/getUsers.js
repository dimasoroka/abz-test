import axios from 'axios';

const getUsers = async (count) => {
  return await axios
    .get(
      `https://frontend-test-assignment-api.abz.agency/api/v1/users?page=1&count=${count}`
    )
    .then((res) => res.data);
};

export default getUsers;
