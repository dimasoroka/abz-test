import axios from 'axios';

const getToken = async () => {
  return await axios
    .get('https://frontend-test-assignment-api.abz.agency/api/v1/token')
    .then((response) => response.data.token);
};

export default getToken;
